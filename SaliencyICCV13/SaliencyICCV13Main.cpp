// SaliencyICCV13.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include "CmSaliencyGC.h"

int main(int argc, char* argv[])
{
	return CmSaliencyGC::Demo("C:/Data/SaliencyFT/"); // (un)comment `#define _GET_CSD' in "CmSaliencyGC.cpp":Line 3 for trade off between speed and accuracy
}

